(define-module (gnu packages next-browser)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix licenses)
  #:use-module (gnu packages lisp)
  #:use-module (gnu packages webkit))

(define-public next-browser
  (package
   (name "next-browser")
   (version "0.07")
   (source (origin
	    (method url-fetch)
	    (uri (string-append "https://github.com/next-browser/next/archive/" version ".tar.gz"))
	    (sha256
	     (base32
	      "1nw1qszp3lf7fclmqfjzvsq41wdwghxy6q2sdaq5gbsqg772nk4w"))
	    (file (string-append "next-broswer" version ".tar.gz"))))))


(define-public sbcl-webkitgtk
  (let ((commit "cd2a9008e0c152e54755e8a7f07b050fe36bab31")))
  (package
   (name "sbcl-webkitgtk")
   (version (git-version "2.4" "1" commit))
   (source (origin
	     (method git-fetch)
	     (uri (git-reference
		   (url "https://github.com/next-browser/cl-webkit.git")
		   (commit commit))))
	   (sha256
	    (base32
	     "0f5lyn9i7xrn3g1bddga377mcbawkbxydijpg389q4n04gqj0vwf"))
	   (file-name (git-file-name name version)))))

(define-public sbcl-cffi-gtk
  (package
   (name "sbcl-gtk")))

(define-public sbcl-cffi-sys
  (package
   (name "sbcl-cffi-sys")))

(define-public sbcl-cffi
  (package
   (name "sbcl-cffi")))
